package handlers

import (
	"context"
	"errors"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateAdministration godoc
// @ID create_Administration
// @Router /superAdminORmanager/administration [POST]
// @Summary Create Administration
// @Description  Create Administration
// @Tags Administration
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param profile body admin_service.CreateAdministration true "CreateAdministrationBody"
// @Success 200 {object} http.Response{data=admin_service.Administration} "GetAdministrationBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateAdministration(c *gin.Context) {

	var (
		Administration admin_service.CreateAdministration
		login          = c.Query("login")
		password       = c.Query("password")
	)
	err := c.ShouldBindJSON(&Administration)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}
	//------------------------------------------------------------------
	// check to match branch
	usResp, err := h.services.UserBranchService().GetByID(
		context.Background(),
		&admin_service.UserBranchPrimaryKey{Login: login, Password: password},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	} else if usResp.BranchId != Administration.BranchId {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("branch does not to match"))
		return
	}
	//-------------------------------------------------------------------------------

	// insert user branch table
	_, err = h.services.UserBranchService().Create(
		context.Background(),
		&admin_service.CreateUserBranch{BranchId: Administration.BranchId, Login: Administration.Login, Password: Administration.Password},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("login is need unique"))
		return
	}

	resp, err := h.services.AdministrationService().Create(
		c.Request.Context(),
		&Administration,
	)

	if err != nil {
		_, err = h.services.UserBranchService().Delete(
			context.Background(),
			&admin_service.UserBranchPrimaryKey{Login: Administration.Login, Password: Administration.Password},
		)
		if err != nil {
			h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
			return
		}
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// GetAdministrationByID godoc
// @ID get_Administration_by_id
// @Router /superAdminORmanager/administration/{id} [GET]
// @Summary Get Administration  By ID
// @Description Get Administration  By ID
// @Tags Administration
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=admin_service.Administration} "AdministrationBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetAdministrationByID(c *gin.Context) {

	AdministrationID := c.Param("id")

	if !util.IsValidUUID(AdministrationID) {
		h.handlerResponse(c, "Administration id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Administration id"))
		return
	}

	resp, err := h.services.AdministrationService().GetByID(
		context.Background(),
		&admin_service.AdministrationPrimaryKey{
			Id: AdministrationID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id Administration resposne", http.StatusOK, resp)
}

// GetAdministrationList godoc
// @ID get_Administration_list
// @Router /superAdminORmanager/administration [GET]
// @Summary Get Administration s List
// @Description  Get Administration s List
// @Tags Administration
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=admin_service.GetListAdministrationResponse} "GetAllAdministrationResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetAdministrationList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.AdministrationService().GetList(
		context.Background(),
		&admin_service.GetListAdministrationRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "error while get list administration", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}

// UpdateAdministration godoc
// @ID update_Administration
// @Router /superAdminORmanager/administration/{id} [PUT]
// @Summary Update Administration
// @Description Update Administration
// @Tags Administration
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Param profile body admin_service.UpdateAdministration true "UpdateAdministrationRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Administration} "Administration data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateAdministration(c *gin.Context) {

	var Administration admin_service.UpdateAdministration
	id := c.Param("id")

	if !util.IsValidUUID(id) {
		h.handlerResponse(c, "Administration id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Administration id"))
		return
	}

	err := c.ShouldBindJSON(&Administration)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Administration.Id = id
	resp, err := h.services.AdministrationService().Update(
		c.Request.Context(),
		&Administration,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// DeleteAdministration godoc
// @ID delete_Administration
// @Router /superAdminORmanager/administration/{id} [DELETE]
// @Summary Delete Administration
// @Description Delete Administration
// @Tags Administration
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Administration data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteAdministration(c *gin.Context) {

	AdministrationId := c.Param("id")

	if !util.IsValidUUID(AdministrationId) {
		h.handlerResponse(c, "Administration id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Administration id"))
		return
	}

	resp, err := h.services.AdministrationService().Delete(
		c.Request.Context(),
		&admin_service.AdministrationPrimaryKey{Id: AdministrationId},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
