package handlers

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"user_interest/ui_go_api_gateway/genproto/admin_service"
	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// GetStudent godoc
// @ID get_Student_by_branch_id
// @Router /student/get_student/{branch_id} [GET]
// @Summary Get Student Report By branch_id
// @Description Get Student Report By branch_id
// @Tags Student Report
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param branch_id query string true "branch_id"
// @Success 200 {object} http.Response{data=admin_service.StudentList} "Student Request"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetStudent(c *gin.Context) {
	Id := c.Query("branch_id")
	if !util.IsValidUUID(Id) {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("branch id is an invalid uuid"))
		return
	}

	resp, err := h.services.ReportService().GetStudent(
		context.Background(),
		&admin_service.ReportRequest{
			BranchId: Id,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "error", http.StatusOK, resp)
}

// GetSupportTeacher godoc
// @ID get_SupportTeacher_by_branch_id
// @Router /teacherORsuppTeach/get_support_teacher/{branch_id} [GET]
// @Summary Get get_support_teacher Report By branch_id
// @Description get_support_teacher Report By branch_id
// @Tags get_support_teacher Report
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param branch_id query string true "branch_id"
// @Success 200 {object} http.Response{data=admin_service.SupportTeacherList} "SupportTeacher Request"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSupportTeacher(c *gin.Context) {
	Id := c.Query("branch_id")
	if !util.IsValidUUID(Id) {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("branch id is an invalid uuid"))
		return
	}

	resp, err := h.services.ReportService().GetSupportTeacher(
		context.Background(),
		&admin_service.ReportRequest{
			BranchId: Id,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "error", http.StatusOK, resp)
}

// GetAdministrator godoc
// @ID get_administrator_by_branch_id
// @Router /get_admin/{branch_id} [GET]
// @Summary Get get_admin Report By branch_id
// @Description get_admin Report By branch_id
// @Tags get_admin Report
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param branch_id query string true "branch_id"
// @Success 200 {object} http.Response{data=admin_service.AdminList} "Admin Request"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetAdministrator(c *gin.Context) {
	Id := c.Query("branch_id")
	if !util.IsValidUUID(Id) {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("administration id is an invalid uuid"))
		return
	}

	resp, err := h.services.ReportService().GetAdministrator(
		context.Background(),
		&admin_service.ReportRequest{
			BranchId: Id,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "error", http.StatusOK, resp)
}

// GetTeacher godoc
// @ID get_teacher_by_branch_id
// @Router /teacher/get_teacher/{branch_id} [GET]
// @Summary Get teacher Report By branch_id
// @Description Get teacher Report By branch_id
// @Tags teacher Report
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param branch_id query string true "branch_id"
// @Success 200 {object} http.Response{data=admin_service.TeacherReportList} "teacher Request"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetTeacher(c *gin.Context) {
	Id := c.Query("branch_id")
	if !util.IsValidUUID(Id) {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("teacher id is an invalid uuid"))
		return
	}

	resp, err := h.services.ReportService().GetTeacher(
		context.Background(),
		&admin_service.ReportRequest{
			BranchId: Id,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "error", http.StatusOK, resp)
}

// TeacherReport godoc
// @ID get_teacher_personal_by_id
// @Router /teacher/get_teacher_personal/{teacher_id} [GET]
// @Summary Get teacher Report By teacher_id
// @Description Get teacher Report By teacher_id
// @Tags teacher Report
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param teacher_id query string true "teacher_id"
// @Success 200 {object} http.Response{data=admin_service.TeacherArrList} "teacher Request"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) TeacherReport(c *gin.Context) {
	Id := c.Query("teacher_id")

	if !util.IsValidUUID(Id) {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("teacher id is an invalid uuid"))
		return
	}

	resp, err := h.services.ReportService().TeacherReport(
		context.Background(),
		&admin_service.TeacherId{
			Id: Id,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "error", http.StatusOK, resp)
}

// SupportTeacherReport godoc
// @ID get_pesonal_support_teacher_by_id
// @Router /teacherORsuppTeach/get_pesonal_support_teacher_by_id/{support_teacher_id} [GET]
// @Summary Get support teacher Report By support_teacher_id
// @Description Get support teacher Report By support_teacher_id
// @Tags support teacher Report
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param support_teacher_id query string true "support_teacher_id"
// @Success 200 {object} http.Response{data=admin_service.SupportArrList} " support teacher Request"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) SupportTeacherReport(c *gin.Context) {
	Id := c.Param("support_teacher_id")

	// fmt.Println("OKOKOKO")
	// if !util.IsValidUUID(Id) {
	// 	h.handlerResponse(c, "error teacher id is an invalid uuid", http.StatusBadRequest, errors.New("teacher id is an invalid uuid"))
	// 	return
	// }
	fmt.Println("NOOOOOOOOOOOOOOOOOOOOOOOOOOO")

	resp, err := h.services.ReportService().SupportTeacherReport(
		context.Background(),
		&admin_service.SupportTeacherId{
			SupportTeacherId: Id,
		},
	)
	fmt.Println("______________BUG______1")
	if err != nil {
		fmt.Println("______________BUG________2")
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "error", http.StatusOK, resp)
}

// StudentReport godoc
// @ID get_student_personal_by_id
// @Router /student/get_student_personal/{student_id} [GET]
// @Summary Get teacher Report By id
// @Description Get teacher Report By id
// @Tags teacher Report
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param student_id query string true "student_id"
// @Success 200 {object} http.Response{data=admin_service.StudentArrList} "student Request"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) StudentReport(c *gin.Context) {
	Id := c.Query("student_id")
	fmt.Println(Id)

	if !util.IsValidUUID(Id) {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("student id is an invalid uuid"))
		return
	}

	resp, err := h.services.ReportService().StudentReport(
		context.Background(),
		&admin_service.StudentId{
			StudentId: Id,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "error", http.StatusOK, resp)
}
