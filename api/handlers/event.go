package handlers

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateEvent godoc
// @ID create_Event
// @Router /administration/event [POST]
// @Summary Create Event
// @Description  Create Event
// @Tags Event
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param profile body admin_service.CreateEvent true "CreateEventRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Event} "GetEventBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateEvent(c *gin.Context) {

	var (
		Event    admin_service.CreateEvent
		login    = c.Query("login")
		password = c.Query("password")
	)
	err := c.ShouldBindJSON(&Event)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	//------------------------------------------------------------------
	// check to match branch
	usResp, err := h.services.UserBranchService().GetByID(
		context.Background(),
		&admin_service.UserBranchPrimaryKey{Login: login, Password: password},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	} else if usResp.BranchId != Event.BranchId {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("branch does not to match"))
		return
	}
	//-------------------------------------------------------------------------------

	// check event.day == Sunday
	day, err := time.Parse("2006-01-02", Event.Day)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}
	fmt.Println("_____-----_____	", day.Weekday().String())

	if day.Weekday().String() != "Sunday" {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("only Sunday do event"))
		return
	}

	// check not have event this time
	eventResp, err := h.services.EventService().GetList(
		context.Background(),
		&admin_service.GetListEventRequest{},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	t1, _ := time.Parse("15:04:05", Event.StartTime)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}
	for _, v := range eventResp.Events {
		t2, _ := time.Parse("15:04:05", v.StartTime)
		if err != nil {
			h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
			return
		}
		if t1.String() == t2.String() && v.BranchId == Event.BranchId {
			h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("only one event a day is possible"))
			return
		}
	}

	resp, err := h.services.EventService().Create(
		c.Request.Context(),
		&Event,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// GetEventByID godoc
// @ID get_Event_by_id
// @Router /administration/event/{id} [GET]
// @Summary Get Event  By ID
// @Description Get Event  By ID
// @Tags Event
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=admin_service.Event} "EventBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetEventByID(c *gin.Context) {

	EventID := c.Param("id")

	if !util.IsValidUUID(EventID) {
		h.handlerResponse(c, "Event id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Event id"))
		return
	}

	resp, err := h.services.EventService().GetByID(
		context.Background(),
		&admin_service.EventPrimaryKey{
			Id: EventID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id Event resposne", http.StatusOK, resp)
}

// GetEventList godoc
// @ID get_Event_list
// @Router /administration/event [GET]
// @Summary Get Event s List
// @Description  Get Event s List
// @Tags Event
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=admin_service.GetListEventResponse} "GetAllEventResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetEventList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.EventService().GetList(
		context.Background(),
		&admin_service.GetListEventRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}

// UpdateEvent godoc
// @ID update_Event
// @Router /administration/event/{id} [PUT]
// @Summary Update Event
// @Description Update Event
// @Tags Event
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Param profile body admin_service.UpdateEvent true "UpdateEventRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Event} "Event data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateEvent(c *gin.Context) {

	var Event admin_service.UpdateEvent
	id := c.Param("id")

	if !util.IsValidUUID(id) {
		h.handlerResponse(c, "Event id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Event id"))
		return
	}

	err := c.ShouldBindJSON(&Event)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Event.Id = id
	resp, err := h.services.EventService().Update(
		c.Request.Context(),
		&Event,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// DeleteEvent godoc
// @ID delete_Event
// @Router /administration/event/{id} [DELETE]
// @Summary Delete Event
// @Description Delete Event
// @Tags Event
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Event data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteEvent(c *gin.Context) {

	EventId := c.Param("id")

	if !util.IsValidUUID(EventId) {
		h.handlerResponse(c, "Event id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Event id"))
		return
	}

	resp, err := h.services.EventService().Delete(
		c.Request.Context(),
		&admin_service.EventPrimaryKey{Id: EventId},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
