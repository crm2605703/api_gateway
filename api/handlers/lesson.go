package handlers

import (
	"context"
	"errors"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// CreateLesson godoc
// @ID create_Lesson
// @Router /teacher/lesson [POST]
// @Summary Create Lesson
// @Description  Create Lesson
// @Tags Lesson
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param profile body admin_service.CreateLesson true "CreateLessonRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Lesson} "GetLessonBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateLesson(c *gin.Context) {

	var Lesson admin_service.CreateLesson
	err := c.ShouldBindJSON(&Lesson)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.LessonService().Create(
		c.Request.Context(),
		&Lesson,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetLessonByID godoc
// @ID get_Lesson_by_id
// @Router /teacherORsuppTeach/lesson/{id} [GET]
// @Summary Get Lesson  By ID
// @Description Get Lesson  By ID
// @Tags Lesson
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=admin_service.Lesson} "LessonBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetLessonByID(c *gin.Context) {

	LessonID := c.Param("id")

	if !util.IsValidUUID(LessonID) {
		h.handlerResponse(c, "Lesson id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Lesson id"))
		return
	}

	resp, err := h.services.LessonService().GetByID(
		context.Background(),
		&admin_service.LessonPrimaryKey{
			Id: LessonID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id Lesson resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetLessonList godoc
// @ID get_Lesson_list
// @Router /teacherORsuppTeach/lesson [GET]
// @Summary Get Lesson s List
// @Description  Get Lesson s List
// @Tags Lesson
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=admin_service.GetListLessonResponse} "GetAllLessonResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetLessonList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.LessonService().GetList(
		context.Background(),
		&admin_service.GetListLessonRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateLesson godoc
// @ID update_Lesson
// @Router /teacher/lesson/{id} [PUT]
// @Summary Update Lesson
// @Description Update Lesson
// @Tags Lesson
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Param profile body admin_service.UpdateLesson true "UpdateLessonRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Lesson} "Lesson data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateLesson(c *gin.Context) {

	var Lesson admin_service.UpdateLesson
	id := c.Param("id")

	if !util.IsValidUUID(id) {
		h.handlerResponse(c, "Lesson id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Lesson id"))
		return
	}

	err := c.ShouldBindJSON(&Lesson)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Lesson.Id = id
	resp, err := h.services.LessonService().Update(
		c.Request.Context(),
		&Lesson,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// DeleteLesson godoc
// @ID delete_Lesson
// @Router /teacher/lesson/{id} [DELETE]
// @Summary Delete Lesson
// @Description Delete Lesson
// @Tags Lesson
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Lesson data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteLesson(c *gin.Context) {

	LessonId := c.Param("id")

	if !util.IsValidUUID(LessonId) {
		h.handlerResponse(c, "Lesson id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Lesson id"))
		return
	}

	resp, err := h.services.LessonService().Delete(
		c.Request.Context(),
		&admin_service.LessonPrimaryKey{Id: LessonId},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
