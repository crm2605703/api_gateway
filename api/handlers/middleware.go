package handlers

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"github.com/gin-gonic/gin"
)

func (h *Handler) SuperUserCheck() gin.HandlerFunc {

	return func(c *gin.Context) {
		var (
			login    = c.Query("login")
			password = c.Query("password")
		)
		// check super user
		if login != h.cfg.SuperLogin || password != h.cfg.SuperPassword {
			c.AbortWithError(http.StatusForbidden, errors.New("incorrect login or password"))
			return
		}

		c.Next()
		return
	}
}

func (h *Handler) SuperAdminCheck() gin.HandlerFunc {

	return func(c *gin.Context) {

		var (
			login    = c.Query("login")
			password = c.Query("password")
		)

		// check super admin
		superAdminResp, err := h.services.SuperAdminService().GetList(
			context.Background(),
			&admin_service.GetListSuperAdminRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, err)
			return
		}

		for _, v := range superAdminResp.SuperAdmins {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}

		c.AbortWithError(http.StatusForbidden, errors.New("permission not available"))
	}
}

func (h *Handler) SuperAdminOrManagerCheck() gin.HandlerFunc {

	return func(c *gin.Context) {
		var (
			login    = c.Query("login")
			password = c.Query("password")
		)

		// check super admin
		superAdminResp, err := h.services.SuperAdminService().GetList(
			context.Background(),
			&admin_service.GetListSuperAdminRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range superAdminResp.SuperAdmins {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		//--------------------------------------------------------------------
		// check manager
		managerResp, err := h.services.ManagerService().GetList(
			context.Background(),
			&admin_service.GetListManagerRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range managerResp.Managers {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}

		c.AbortWithError(http.StatusForbidden, errors.New("permission not available"))
	}
}

func (h *Handler) TeacherCheck() gin.HandlerFunc {

	return func(c *gin.Context) {
		var (
			login    = c.Query("login")
			password = c.Query("password")
		)

		// check super admin
		superAdminResp, err := h.services.SuperAdminService().GetList(
			context.Background(),
			&admin_service.GetListSuperAdminRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range superAdminResp.SuperAdmins {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		//----------------------------------------------------------------------
		// check teacher
		teacherResp, err := h.services.TeacherService().GetList(
			context.Background(),
			&admin_service.GetListTeacherRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range teacherResp.Teachers {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		c.AbortWithError(http.StatusForbidden, errors.New("permission not available"))
	}
}

func (h *Handler) TeacherOrSupprtCheck() gin.HandlerFunc {

	return func(c *gin.Context) {
		var (
			login    = c.Query("login")
			password = c.Query("password")
		)
		// fmt.Println("____________BUG", login, password)
		// check super admin
		superAdminResp, err := h.services.SuperAdminService().GetList(
			context.Background(),
			&admin_service.GetListSuperAdminRequest{},
		)
		// fmt.Println(superAdminResp)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range superAdminResp.SuperAdmins {
			// fmt.Println("-------	", v.Login, login, v.Password, password)
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		//--------------------------------------------------------------------
		// check teacher
		teacherResp, err := h.services.TeacherService().GetList(
			context.Background(),
			&admin_service.GetListTeacherRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range teacherResp.Teachers {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		//------------------------------------------------------------------------
		// check support teacher
		supteacherResp, err := h.services.SupportTeacherService().GetList(
			context.Background(),
			&admin_service.GetListSupportTeacherRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range supteacherResp.SupportTeachers {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		// fmt.Println("____________BUG", login, password)

		c.AbortWithError(http.StatusForbidden, errors.New("permission not available"))
	}
}

func (h *Handler) Student() gin.HandlerFunc {

	return func(c *gin.Context) {
		var (
			login    = c.Query("login")
			password = c.Query("password")
		)

		// check super admin
		superAdminResp, err := h.services.SuperAdminService().GetList(
			context.Background(),
			&admin_service.GetListSuperAdminRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range superAdminResp.SuperAdmins {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		//------------------------------------------------------------------------
		// check student
		studentResp, err := h.services.StudentService().GetList(
			context.Background(),
			&admin_service.GetListStudentRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range studentResp.Students {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		c.AbortWithError(http.StatusForbidden, errors.New("permission not available"))
	}
}

func (h *Handler) AdministrationCheck() gin.HandlerFunc {

	return func(c *gin.Context) {
		var (
			login    = c.Query("login")
			password = c.Query("password")
		)
		// check administration
		administrationResp, err := h.services.AdministrationService().GetList(
			context.Background(),
			&admin_service.GetListAdministrationRequest{},
		)
		fmt.Println("________BUG__________	", login, password)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range administrationResp.Administrations {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		//------------------------------------------------------------------------
		// check super admin
		superAdminResp, err := h.services.SuperAdminService().GetList(
			context.Background(),
			&admin_service.GetListSuperAdminRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range superAdminResp.SuperAdmins {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		c.AbortWithError(http.StatusForbidden, errors.New("permission not available"))
	}
}

func (h *Handler) ManagerORteacherCheck() gin.HandlerFunc {

	return func(c *gin.Context) {
		var (
			login    = c.Query("login")
			password = c.Query("password")
		)
		// check super admin
		superAdminResp, err := h.services.SuperAdminService().GetList(
			context.Background(),
			&admin_service.GetListSuperAdminRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range superAdminResp.SuperAdmins {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		//------------------------------------------------------------------------
		// check teacher
		teacherResp, err := h.services.TeacherService().GetList(
			context.Background(),
			&admin_service.GetListTeacherRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range teacherResp.Teachers {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		//------------------------------------------------------------------------
		managerResp, err := h.services.ManagerService().GetList(
			context.Background(),
			&admin_service.GetListManagerRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range managerResp.Managers {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}

		c.AbortWithError(http.StatusForbidden, errors.New("permission not available"))
	}
}

func (h *Handler) MTSCheck() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			login    = c.Query("login")
			password = c.Query("password")
		)
		// check super admin
		superAdminResp, err := h.services.SuperAdminService().GetList(
			context.Background(),
			&admin_service.GetListSuperAdminRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range superAdminResp.SuperAdmins {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		//------------------------------------------------------------------------
		// check manager
		managerResp, err := h.services.ManagerService().GetList(
			context.Background(),
			&admin_service.GetListManagerRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range managerResp.Managers {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		//------------------------------------------------------------------------
		// check teacher
		teacherResp, err := h.services.TeacherService().GetList(
			context.Background(),
			&admin_service.GetListTeacherRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range teacherResp.Teachers {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		// check student
		//------------------------------------------------------------------------
		studentResp, err := h.services.StudentService().GetList(
			context.Background(),
			&admin_service.GetListStudentRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range studentResp.Students {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}

		c.AbortWithError(http.StatusForbidden, errors.New("permission not available"))
	}
}

func (h *Handler) MTACheck() gin.HandlerFunc {

	return func(c *gin.Context) {
		var (
			login    = c.Query("login")
			password = c.Query("password")
		)
		// check super admin
		superAdminResp, err := h.services.SuperAdminService().GetList(
			context.Background(),
			&admin_service.GetListSuperAdminRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range superAdminResp.SuperAdmins {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		//-------------------------------------------------------------------------------
		// check manager
		managerResp, err := h.services.ManagerService().GetList(
			context.Background(),
			&admin_service.GetListManagerRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range managerResp.Managers {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		//-------------------------------------------------------------------------------
		// check teacher
		teacherResp, err := h.services.TeacherService().GetList(
			context.Background(),
			&admin_service.GetListTeacherRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range teacherResp.Teachers {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}
		//-------------------------------------------------------------------------------
		// check administration
		administrationResp, err := h.services.AdministrationService().GetList(
			context.Background(),
			&admin_service.GetListAdministrationRequest{},
		)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, errors.New("login or password wrong"))
			return
		}

		for _, v := range administrationResp.Administrations {
			if v.Login == login && v.Password == password {
				c.Next()
				return
			}
		}

		c.AbortWithError(http.StatusForbidden, errors.New("permission not available"))
	}
}
