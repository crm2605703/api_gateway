package handlers

import (
	"context"
	"errors"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateSupportTeacher godoc
// @ID create_SupportTeacher
// @Router /superAdminORmanager/support_teacher [POST]
// @Summary Create SupportTeacher
// @Description  Create SupportTeacher
// @Tags SupportTeacher
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param profile body admin_service.CreateSupportTeacher true "CreateSupportTeacherRequestBody"
// @Success 200 {object} http.Response{data=admin_service.SupportTeacher} "GetSupportTeacherBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSupportTeacher(c *gin.Context) {

	var (
		SupportTeacher admin_service.CreateSupportTeacher
		login          = c.Query("login")
		password       = c.Query("password")
	)
	err := c.ShouldBindJSON(&SupportTeacher)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	//------------------------------------------------------------------
	// check to match branch
	usResp, err := h.services.UserBranchService().GetByID(
		context.Background(),
		&admin_service.UserBranchPrimaryKey{Login: login, Password: password},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	} else if usResp.BranchId != SupportTeacher.BranchId {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("branch does not to match"))
		return
	}
	//-------------------------------------------------------------------------------

	// insert user branch table
	_, err = h.services.UserBranchService().Create(
		context.Background(),
		&admin_service.CreateUserBranch{BranchId: SupportTeacher.BranchId, Login: SupportTeacher.Login, Password: SupportTeacher.Password},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("login is need unique"))
		return
	}

	resp, err := h.services.SupportTeacherService().Create(
		c.Request.Context(),
		&SupportTeacher,
	)

	if err != nil {
		_, err = h.services.UserBranchService().Delete(
			context.Background(),
			&admin_service.UserBranchPrimaryKey{Login: SupportTeacher.Login, Password: SupportTeacher.Password},
		)
		if err != nil {
			h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
			return
		}
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// GetSupportTeacherByID godoc
// @ID get_SupportTeacher_by_id
// @Router /superAdminORmanager/support_teacher/{id} [GET]
// @Summary Get SupportTeacher  By ID
// @Description Get SupportTeacher  By ID
// @Tags SupportTeacher
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=admin_service.SupportTeacher} "SupportTeacherBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSupportTeacherByID(c *gin.Context) {

	SupportTeacherID := c.Param("id")

	if !util.IsValidUUID(SupportTeacherID) {
		h.handlerResponse(c, "SupportTeacher id is an invalid uuid", http.StatusBadRequest, errors.New("invalid SupportTeacher id"))
		return
	}

	resp, err := h.services.SupportTeacherService().GetByID(
		context.Background(),
		&admin_service.SupportTeacherPrimaryKey{
			Id: SupportTeacherID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id SupportTeacher resposne", http.StatusOK, resp)
}

// GetSupportTeacherList godoc
// @ID get_SupportTeacher_list
// @Router /superAdminORmanager/support_teacher [GET]
// @Summary Get SupportTeacher s List
// @Description  Get SupportTeacher s List
// @Tags SupportTeacher
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=admin_service.GetListSupportTeacherResponse} "GetAllSupportTeacherResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSupportTeacherList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SupportTeacherService().GetList(
		context.Background(),
		&admin_service.GetListSupportTeacherRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}

// UpdateSupportTeacher godoc
// @ID update_SupportTeacher
// @Router /superAdminORmanager/support_teacher/{id} [PUT]
// @Summary Update SupportTeacher
// @Description Update SupportTeacher
// @Tags SupportTeacher
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Param profile body admin_service.UpdateSupportTeacher true "UpdateSupportTeacherRequestBody"
// @Success 200 {object} http.Response{data=admin_service.SupportTeacher} "SupportTeacher data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSupportTeacher(c *gin.Context) {

	var SupportTeacher admin_service.UpdateSupportTeacher
	id := c.Param("id")

	if !util.IsValidUUID(id) {
		h.handlerResponse(c, "SupportTeacher id is an invalid uuid", http.StatusBadRequest, errors.New("invalid SupportTeacher id"))
		return
	}

	err := c.ShouldBindJSON(&SupportTeacher)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	SupportTeacher.Id = id
	resp, err := h.services.SupportTeacherService().Update(
		c.Request.Context(),
		&SupportTeacher,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// DeleteSupportTeacher godoc
// @ID delete_SupportTeacher
// @Router /superAdminORmanager/support_teacher/{id} [DELETE]
// @Summary Delete SupportTeacher
// @Description Delete SupportTeacher
// @Tags SupportTeacher
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "SupportTeacher data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSupportTeacher(c *gin.Context) {

	SupportTeacherId := c.Param("id")

	if !util.IsValidUUID(SupportTeacherId) {
		h.handlerResponse(c, "SupportTeacher id is an invalid uuid", http.StatusBadRequest, errors.New("invalid SupportTeacher id"))
		return
	}

	resp, err := h.services.SupportTeacherService().Delete(
		c.Request.Context(),
		&admin_service.SupportTeacherPrimaryKey{Id: SupportTeacherId},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
