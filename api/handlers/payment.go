package handlers

import (
	"context"
	"errors"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreatePayment godoc
// @ID create_Payment
// @Router /administration/payment [POST]
// @Summary Create Payment
// @Description  Create Payment
// @Tags Payment
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param profile body admin_service.CreatePayment true "CreatePaymentRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Payment} "GetPaymentBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreatePayment(c *gin.Context) {

	var Payment admin_service.CreatePayment

	err := c.ShouldBindJSON(&Payment)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.PaymentService().Create(
		c.Request.Context(),
		&Payment,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// GetPaymentByID godoc
// @ID get_Payment_by_id
// @Router /administration/payment/{id} [GET]
// @Summary Get Payment  By ID
// @Description Get Payment  By ID
// @Tags Payment
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=admin_service.Payment} "PaymentBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetPaymentByID(c *gin.Context) {

	PaymentID := c.Param("id")
	// fmt.Println("------------------------	", PaymentID)

	if !util.IsValidUUID(PaymentID) {
		h.handlerResponse(c, "Payment id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Payment id"))
		return
	}

	resp, err := h.services.PaymentService().GetByID(
		context.Background(),
		&admin_service.PaymentPrimaryKey{
			Id: PaymentID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id Payment resposne", http.StatusOK, resp)
}

// GetPaymentList godoc
// @ID get_Payment_list
// @Router /administration/payment [GET]
// @Summary Get Payment s List
// @Description  Get Payment s List
// @Tags Payment
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=admin_service.GetListPaymentResponse} "GetAllPaymentResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetPaymentList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.PaymentService().GetList(
		context.Background(),
		&admin_service.GetListPaymentRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}

// UpdatePayment godoc
// @ID update_Payment
// @Router /administration/payment/{id} [PUT]
// @Summary Update Payment
// @Description Update Payment
// @Tags Payment
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Param profile body admin_service.UpdatePayment true "UpdatePaymentRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Payment} "Payment data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePayment(c *gin.Context) {

	var Payment admin_service.UpdatePayment
	id := c.Param("id")

	if !util.IsValidUUID(id) {
		h.handlerResponse(c, "Payment id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Payment id"))
		return
	}

	err := c.ShouldBindJSON(&Payment)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Payment.Id = id
	resp, err := h.services.PaymentService().Update(
		c.Request.Context(),
		&Payment,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// DeletePayment godoc
// @ID delete_Payment
// @Router /administration/payment/{id} [DELETE]
// @Summary Delete Payment
// @Description Delete Payment
// @Tags Payment
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Payment data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeletePayment(c *gin.Context) {

	PaymentId := c.Param("id")

	if !util.IsValidUUID(PaymentId) {
		h.handlerResponse(c, "Payment id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Payment id"))
		return
	}

	resp, err := h.services.PaymentService().Delete(
		c.Request.Context(),
		&admin_service.PaymentPrimaryKey{Id: PaymentId},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
