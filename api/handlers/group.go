package handlers

import (
	"context"
	"errors"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateGroup godoc
// @ID create_Group
// @Router /superAdminORmanager/group [POST]
// @Summary Create Group
// @Description  Create Group
// @Tags Group
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param profile body admin_service.CreateGroup true "CreateGroupRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Group} "GetGroupBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateGroup(c *gin.Context) {

	var (
		Group    admin_service.CreateGroup
		login    = c.Query("login")
		password = c.Query("password")
	)

	err := c.ShouldBindJSON(&Group)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	//------------------------------------------------------------------
	// check to match branch
	usResp, err := h.services.UserBranchService().GetByID(
		context.Background(),
		&admin_service.UserBranchPrimaryKey{Login: login, Password: password},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	} else if usResp.BranchId != Group.BranchId {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("branch does not to match"))
		return
	}
	//-------------------------------------------------------------------------------

	resp, err := h.services.GroupService().Create(
		c.Request.Context(),
		&Group,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// GetGroupByID godoc
// @ID get_Group_by_id
// @Router /group/{id} [GET]
// @Summary Get Group  By ID
// @Description Get Group  By ID
// @Tags Group
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=admin_service.Group} "GroupBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetGroupByID(c *gin.Context) {

	GroupID := c.Param("id")

	if !util.IsValidUUID(GroupID) {
		h.handlerResponse(c, "Group id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Group id"))
		return
	}

	resp, err := h.services.GroupService().GetByID(
		context.Background(),
		&admin_service.GroupPrimaryKey{
			Id: GroupID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id Group resposne", http.StatusOK, resp)
}

// GetGroupList godoc
// @ID get_Group_list
// @Router /group [GET]
// @Summary Get Group s List
// @Description  Get Group s List
// @Tags Group
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=admin_service.GetListGroupResponse} "GetAllGroupResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetGroupList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.GroupService().GetList(
		context.Background(),
		&admin_service.GetListGroupRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}

// UpdateGroup godoc
// @ID update_Group
// @Router /superAdminORmanager/group/{id} [PUT]
// @Summary Update Group
// @Description Update Group
// @Tags Group
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Param profile body admin_service.UpdateGroup true "UpdateGroupRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Group} "Group data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateGroup(c *gin.Context) {

	var Group admin_service.UpdateGroup
	id := c.Param("id")

	if !util.IsValidUUID(id) {
		h.handlerResponse(c, "Group id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Group id"))
		return
	}

	err := c.ShouldBindJSON(&Group)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Group.Id = id
	resp, err := h.services.GroupService().Update(
		c.Request.Context(),
		&Group,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// DeleteGroup godoc
// @ID delete_Group
// @Router /superAdminORmanager/group/{id} [DELETE]
// @Summary Delete Group
// @Description Delete Group
// @Tags Group
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Group data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteGroup(c *gin.Context) {

	GroupId := c.Param("id")

	if !util.IsValidUUID(GroupId) {
		h.handlerResponse(c, "Group id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Group id"))
		return
	}

	resp, err := h.services.GroupService().Delete(
		c.Request.Context(),
		&admin_service.GroupPrimaryKey{Id: GroupId},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
