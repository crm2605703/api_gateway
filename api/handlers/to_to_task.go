package handlers

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateToDoTask godoc
// @ID create_ToDoTask
// @Router /teacher/to_do_task [POST]
// @Summary Create ToDoTask
// @Description  Create ToDoTask
// @Tags ToDoTask
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param profile body admin_service.CreateToDoTask true "CreateToDoTaskRequestBody"
// @Success 200 {object} http.Response{data=admin_service.ToDoTask} "GetToDoTaskBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateToDoTask(c *gin.Context) {

	var ToDoTask admin_service.CreateToDoTask

	err := c.ShouldBindJSON(&ToDoTask)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	// check task deadline
	taskResp, err := h.services.TaskService().GetByID(
		context.Background(),
		&admin_service.TaskPrimaryKey{Id: ToDoTask.TaskId},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	deadline, err := time.Parse("2006-01-02", taskResp.Deadline[:10])
	fmt.Println("______________	deadline",taskResp.Deadline[:10])
	if err != nil {
		fmt.Println("______________________BUG__________--")
		h.handlerResponse(c, "error parsing time", http.StatusInternalServerError, err.Error())
		return
	}

	if deadline.Add(time.Hour * 72).Before(time.Now()) {
		ToDoTask.Score = 0
	} else if deadline.Before(time.Now()) {
		ToDoTask.Score /= 2
	}

	resp, err := h.services.ToDoTaskService().Create(
		c.Request.Context(),
		&ToDoTask,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// GetToDoTaskByID godoc
// @ID get_ToDoTask_by_id
// @Router /teacherORsuppTeach/to_do_task/{id} [GET]
// @Summary Get ToDoTask  By ID
// @Description Get ToDoTask  By ID
// @Tags ToDoTask
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=admin_service.ToDoTask} "ToDoTaskBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetToDoTaskByID(c *gin.Context) {

	ToDoTaskID := c.Param("id")

	if !util.IsValidUUID(ToDoTaskID) {
		h.handlerResponse(c, "ToDoTask id is an invalid uuid", http.StatusBadRequest, errors.New("invalid ToDoTask id"))
		return
	}

	resp, err := h.services.ToDoTaskService().GetByID(
		context.Background(),
		&admin_service.ToDoTaskPrimaryKey{
			Id: ToDoTaskID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id ToDoTask resposne", http.StatusOK, resp)
}

// GetToDoTaskList godoc
// @ID get_ToDoTask_list
// @Router /teacherORsuppTeach/to_do_task [GET]
// @Summary Get ToDoTask s List
// @Description  Get ToDoTask s List
// @Tags ToDoTask
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=admin_service.GetListToDoTaskResponse} "GetAllToDoTaskResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetToDoTaskList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ToDoTaskService().GetList(
		context.Background(),
		&admin_service.GetListToDoTaskRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}

// UpdateToDoTask godoc
// @ID update_ToDoTask
// @Router /teacher/to_do_task/{id} [PUT]
// @Summary Update ToDoTask
// @Description Update ToDoTask
// @Tags ToDoTask
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Param profile body admin_service.UpdateToDoTask true "UpdateToDoTaskRequestBody"
// @Success 200 {object} http.Response{data=admin_service.ToDoTask} "ToDoTask data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateToDoTask(c *gin.Context) {

	var ToDoTask admin_service.UpdateToDoTask
	id := c.Param("id")

	if !util.IsValidUUID(id) {
		h.handlerResponse(c, "ToDoTask id is an invalid uuid", http.StatusBadRequest, errors.New("invalid ToDoTask id"))
		return
	}

	err := c.ShouldBindJSON(&ToDoTask)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	ToDoTask.Id = id
	resp, err := h.services.ToDoTaskService().Update(
		c.Request.Context(),
		&ToDoTask,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// DeleteToDoTask godoc
// @ID delete_ToDoTask
// @Router /teacher/to_do_task/{id} [DELETE]
// @Summary Delete ToDoTask
// @Description Delete ToDoTask
// @Tags ToDoTask
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "ToDoTask data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteToDoTask(c *gin.Context) {

	ToDoTaskId := c.Param("id")

	if !util.IsValidUUID(ToDoTaskId) {
		h.handlerResponse(c, "ToDoTask id is an invalid uuid", http.StatusBadRequest, errors.New("invalid ToDoTask id"))
		return
	}

	resp, err := h.services.ToDoTaskService().Delete(
		c.Request.Context(),
		&admin_service.ToDoTaskPrimaryKey{Id: ToDoTaskId},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
