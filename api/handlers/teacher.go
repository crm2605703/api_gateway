package handlers

import (
	"context"
	"errors"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateTeacher godoc
// @ID create_Teacher
// @Router /superAdminORmanager/teacher [POST]
// @Summary Create Teacher
// @Description  Create Teacher
// @Tags Teacher
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param profile body admin_service.CreateTeacher true "CreateTeacherRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Teacher} "GetTeacherBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateTeacher(c *gin.Context) {

	var (
		Teacher  admin_service.CreateTeacher
		login    = c.Query("login")
		password = c.Query("password")
	)
	err := c.ShouldBindJSON(&Teacher)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	//------------------------------------------------------------------
	// check to match branch
	usResp, err := h.services.UserBranchService().GetByID(
		context.Background(),
		&admin_service.UserBranchPrimaryKey{Login: login, Password: password},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	} else if usResp.BranchId != Teacher.BranchId {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("branch does not to match"))
		return
	}
	//-------------------------------------------------------------------------------

	// insert user branch table
	_, err = h.services.UserBranchService().Create(
		context.Background(),
		&admin_service.CreateUserBranch{BranchId: Teacher.BranchId, Login: Teacher.Login, Password: Teacher.Password},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("login is need unique"))
		return
	}

	resp, err := h.services.TeacherService().Create(
		c.Request.Context(),
		&Teacher,
	)

	if err != nil {
		// delete user_branch because teacher did not create
		_, err = h.services.UserBranchService().Delete(
			context.Background(),
			&admin_service.UserBranchPrimaryKey{Login: Teacher.Login, Password: Teacher.Password},
		)
		if err != nil {
			h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
			return
		}
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// GetTeacherByID godoc
// @ID get_Teacher_by_id
// @Router /superAdminORmanager/teacher/{id} [GET]
// @Summary Get Teacher  By ID
// @Description Get Teacher  By ID
// @Tags Teacher
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=admin_service.Teacher} "TeacherBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetTeacherByID(c *gin.Context) {

	TeacherID := c.Param("id")

	if !util.IsValidUUID(TeacherID) {
		h.handlerResponse(c, "Teacher id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Teacher id"))
		return
	}

	resp, err := h.services.TeacherService().GetByID(
		context.Background(),
		&admin_service.TeacherPrimaryKey{
			Id: TeacherID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id Teacher resposne", http.StatusOK, resp)
}

// GetTeacherList godoc
// @ID get_Teacher_list
// @Router /superAdminORmanager/teacher [GET]
// @Summary Get Teacher s List
// @Description  Get Teacher s List
// @Tags Teacher
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=admin_service.GetListTeacherResponse} "GetAllTeacherResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetTeacherList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.TeacherService().GetList(
		context.Background(),
		&admin_service.GetListTeacherRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}

// UpdateTeacher godoc
// @ID update_Teacher
// @Router /superAdminORmanager/teacher/{id} [PUT]
// @Summary Update Teacher
// @Description Update Teacher
// @Tags Teacher
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Param profile body admin_service.UpdateTeacher true "UpdateTeacherRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Teacher} "Teacher data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateTeacher(c *gin.Context) {

	var Teacher admin_service.UpdateTeacher
	id := c.Param("id")

	if !util.IsValidUUID(id) {
		h.handlerResponse(c, "Teacher id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Teacher id"))
		return
	}

	err := c.ShouldBindJSON(&Teacher)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Teacher.Id = id
	resp, err := h.services.TeacherService().Update(
		c.Request.Context(),
		&Teacher,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// DeleteTeacher godoc
// @ID delete_Teacher
// @Router /superAdminORmanager/teacher/{id} [DELETE]
// @Summary Delete Teacher
// @Description Delete Teacher
// @Tags Teacher
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Teacher data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteTeacher(c *gin.Context) {

	TeacherId := c.Param("id")

	if !util.IsValidUUID(TeacherId) {
		h.handlerResponse(c, "Teacher id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Teacher id"))
		return
	}

	resp, err := h.services.TeacherService().Delete(
		c.Request.Context(),
		&admin_service.TeacherPrimaryKey{Id: TeacherId},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
