package handlers

import (
	"context"
	"errors"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// CreateJournal godoc
// @ID create_Journal
// @Router /teacher/journal [POST]
// @Summary Create Journal
// @Description  Create Journal
// @Tags Journal
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param profile body admin_service.CreateJournal true "CreateJournalRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Journal} "GetJournalBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateJournal(c *gin.Context) {

	var Journal admin_service.CreateJournal
	err := c.ShouldBindJSON(&Journal)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.JournalService().Create(
		c.Request.Context(),
		&Journal,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetJournalByID godoc
// @ID get_Journal_by_id
// @Router /manegORteach/journal/{id} [GET]
// @Summary Get Journal  By ID
// @Description Get Journal  By ID
// @Tags Journal
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=admin_service.Journal} "JournalBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetJournalByID(c *gin.Context) {

	JournalID := c.Param("id")

	if !util.IsValidUUID(JournalID) {
		h.handlerResponse(c, "Journal id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Journal id"))
		return
	}

	resp, err := h.services.JournalService().GetByID(
		context.Background(),
		&admin_service.JournalPrimaryKey{
			Id: JournalID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id Journal resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetJournalList godoc
// @ID get_Journal_list
// @Router /manegORteach/journal [GET]
// @Summary Get Journal s List
// @Description  Get Journal s List
// @Tags Journal
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=admin_service.GetListJournalResponse} "GetAllJournalResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetJournalList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.JournalService().GetList(
		context.Background(),
		&admin_service.GetListJournalRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateJournal godoc
// @ID update_Journal
// @Router /teacher/journal/{id} [PUT]
// @Summary Update Journal
// @Description Update Journal
// @Tags Journal
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Param profile body admin_service.UpdateJournal true "UpdateJournalRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Journal} "Journal data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateJournal(c *gin.Context) {

	var Journal admin_service.UpdateJournal
	id := c.Param("id")

	if !util.IsValidUUID(id) {
		h.handlerResponse(c, "Journal id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Journal id"))
		return
	}

	err := c.ShouldBindJSON(&Journal)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Journal.Id = id
	resp, err := h.services.JournalService().Update(
		c.Request.Context(),
		&Journal,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// DeleteJournal godoc
// @ID delete_Journal
// @Router /teacher/journal/{id} [DELETE]
// @Summary Delete Journal
// @Description Delete Journal
// @Tags Journal
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Journal data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteJournal(c *gin.Context) {

	JournalId := c.Param("id")

	if !util.IsValidUUID(JournalId) {
		h.handlerResponse(c, "Journal id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Journal id"))
		return
	}

	resp, err := h.services.JournalService().Delete(
		c.Request.Context(),
		&admin_service.JournalPrimaryKey{Id: JournalId},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
