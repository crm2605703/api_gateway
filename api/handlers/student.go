package handlers

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateStudent godoc
// @ID create_Student
// @Router /mta/student [POST]
// @Summary Create Student
// @Description  Create Student
// @Tags Student
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param profile body admin_service.CreateStudent true "CreateStudentRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Student} "GetStudentBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateStudent(c *gin.Context) {

	var (
		Student  admin_service.CreateStudent
		login    = c.Query("login")
		password = c.Query("password")
	)
	err := c.ShouldBindJSON(&Student)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	//------------------------------------------------------------------
	// check to match branch
	usResp, err := h.services.UserBranchService().GetByID(
		context.Background(),
		&admin_service.UserBranchPrimaryKey{Login: login, Password: password},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	} else if usResp.BranchId != Student.BranchId {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("branch does not to match"))
		return
	}
	//-------------------------------------------------------------------------------

	// insert user branch table
	_, err = h.services.UserBranchService().Create(
		context.Background(),
		&admin_service.CreateUserBranch{BranchId: Student.BranchId, Login: Student.Login, Password: Student.Password},
	)
	if err != nil {
		fmt.Println("_____________WHSTT")
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("login is need unique"))
		return
	}

	resp, err := h.services.StudentService().Create(
		c.Request.Context(),
		&Student,
	)

	if err != nil {
		// insert user branch table
		_, err = h.services.UserBranchService().Delete(
			context.Background(),
			&admin_service.UserBranchPrimaryKey{Login: Student.Login, Password: Student.Password},
		)
		if err != nil {
			h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
			return
		}
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// GetStudentByID godoc
// @ID get_Student_by_id
// @Router /mts/student/{id} [GET]
// @Summary Get Student  By ID
// @Description Get Student  By ID
// @Tags Student
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=admin_service.Student} "StudentBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetStudentByID(c *gin.Context) {

	StudentID := c.Param("id")

	if !util.IsValidUUID(StudentID) {
		h.handlerResponse(c, "Student id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Student id"))
		return
	}

	resp, err := h.services.StudentService().GetByID(
		context.Background(),
		&admin_service.StudentPrimaryKey{
			Id: StudentID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id Student resposne", http.StatusOK, resp)
}

// GetStudentList godoc
// @ID get_Student_list
// @Router /mta/student [GET]
// @Summary Get Student s List
// @Description  Get Student s List
// @Tags Student
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=admin_service.GetListStudentResponse} "GetAllStudentResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetStudentList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.StudentService().GetList(
		context.Background(),
		&admin_service.GetListStudentRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}

// UpdateStudent godoc
// @ID update_Student
// @Router /mta/student/{id} [PUT]
// @Summary Update Student
// @Description Update Student
// @Tags Student
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Param profile body admin_service.UpdateStudent true "UpdateStudentRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Student} "Student data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateStudent(c *gin.Context) {

	var Student admin_service.UpdateStudent
	id := c.Param("id")

	if !util.IsValidUUID(id) {
		h.handlerResponse(c, "Student id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Student id"))
		return
	}

	err := c.ShouldBindJSON(&Student)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Student.Id = id
	resp, err := h.services.StudentService().Update(
		c.Request.Context(),
		&Student,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// DeleteStudent godoc
// @ID delete_Student
// @Router /mta/student/{id} [DELETE]
// @Summary Delete Student
// @Description Delete Student
// @Tags Student
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Student data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteStudent(c *gin.Context) {

	StudentId := c.Param("id")

	if !util.IsValidUUID(StudentId) {
		h.handlerResponse(c, "Student id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Student id"))
		return
	}

	resp, err := h.services.StudentService().Delete(
		c.Request.Context(),
		&admin_service.StudentPrimaryKey{Id: StudentId},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
