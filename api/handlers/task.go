package handlers

import (
	"context"
	"errors"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateTask godoc
// @ID create_Task
// @Router /teacher/task [POST]
// @Summary Create Task
// @Description  Create Task
// @Tags Task
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param profile body admin_service.CreateTask true "CreateTaskRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Task} "GetTaskBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateTask(c *gin.Context) {

	var Task admin_service.CreateTask
	err := c.ShouldBindJSON(&Task)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.TaskService().Create(
		c.Request.Context(),
		&Task,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// GetTaskByID godoc
// @ID get_Task_by_id
// @Router /sp/task/{id} [GET]
// @Summary Get Task  By ID
// @Description Get Task  By ID
// @Tags Task
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=admin_service.Task} "TaskBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetTaskByID(c *gin.Context) {

	TaskID := c.Param("id")

	if !util.IsValidUUID(TaskID) {
		h.handlerResponse(c, "Task id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Task id"))
		return
	}

	resp, err := h.services.TaskService().GetByID(
		context.Background(),
		&admin_service.TaskPrimaryKey{
			Id: TaskID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id Task resposne", http.StatusOK, resp)
}

// GetTaskList godoc
// @ID get_Task_list
// @Router /sp/task [GET]
// @Summary Get Task s List
// @Description  Get Task s List
// @Tags Task
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=admin_service.GetListTaskResponse} "GetAllTaskResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetTaskList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.TaskService().GetList(
		context.Background(),
		&admin_service.GetListTaskRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}

// UpdateTask godoc
// @ID update_Task
// @Router /teacher/task/{id} [PUT]
// @Summary Update Task
// @Description Update Task
// @Tags Task
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Param profile body admin_service.UpdateTask true "UpdateTaskRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Task} "Task data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateTask(c *gin.Context) {

	var Task admin_service.UpdateTask
	id := c.Param("id")

	if !util.IsValidUUID(id) {
		h.handlerResponse(c, "Task id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Task id"))
		return
	}

	err := c.ShouldBindJSON(&Task)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Task.Id = id
	resp, err := h.services.TaskService().Update(
		c.Request.Context(),
		&Task,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// DeleteTask godoc
// @ID delete_Task
// @Router /teacher/task/{id} [DELETE]
// @Summary Delete Task
// @Description Delete Task
// @Tags Task
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Task data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteTask(c *gin.Context) {

	TaskId := c.Param("id")

	if !util.IsValidUUID(TaskId) {
		h.handlerResponse(c, "Task id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Task id"))
		return
	}

	resp, err := h.services.TaskService().Delete(
		c.Request.Context(),
		&admin_service.TaskPrimaryKey{Id: TaskId},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
