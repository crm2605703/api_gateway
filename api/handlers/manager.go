package handlers

import (
	"context"
	"errors"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// @Security ApiKeyAuth
// CreateManager godoc
// @ID create_Manager
// @Router /super_admin/manager [POST]
// @Summary Create Manager
// @Description  Create Manager
// @Tags Manager
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param profile body admin_service.CreateManager true "CreateManagerRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Manager} "GetManagerBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateManager(c *gin.Context) {

	var (
		Manager admin_service.CreateManager
		login    = c.Query("login")
		password = c.Query("password")
	)
	err := c.ShouldBindJSON(&Manager)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	//------------------------------------------------------------------
	// check to match branch
	usResp, err := h.services.UserBranchService().GetByID(
		context.Background(),
		&admin_service.UserBranchPrimaryKey{Login: login, Password: password},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	} else if usResp.BranchId != Manager.BranchId {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("branch does not to match"))
		return
	}
	//-------------------------------------------------------------------------------


	// insert user branch table
	_, err = h.services.UserBranchService().Create(
		context.Background(),
		&admin_service.CreateUserBranch{BranchId: Manager.BranchId, Login: Manager.Login, Password: Manager.Password},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("login is need unique"))
		return
	}

	// check manager existent this branch
	managerListresp, err := h.services.ManagerService().GetList(
		c.Request.Context(),
		&admin_service.GetListManagerRequest{},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}
	for _, v := range managerListresp.Managers {
		if v.BranchId == Manager.BranchId {
			h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("manager existent this branch"))
			return
		}
	}

	resp, err := h.services.ManagerService().Create(
		c.Request.Context(),
		&Manager,
	)

	if err != nil {
		_, err = h.services.UserBranchService().Delete(
			context.Background(),
			&admin_service.UserBranchPrimaryKey{Login: Manager.Login, Password: Manager.Password},
		)
		if err != nil {
			h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
			return
		}
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetManagerByID godoc
// @ID get_Manager_by_id
// @Router /super_admin/manager/{id} [GET]
// @Summary Get Manager  By ID
// @Description Get Manager  By ID
// @Tags Manager
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=admin_service.Manager} "ManagerBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetManagerByID(c *gin.Context) {

	ManagerID := c.Param("id")

	if !util.IsValidUUID(ManagerID) {
		h.handlerResponse(c, "Manager id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Manager id"))
		return
	}

	resp, err := h.services.ManagerService().GetByID(
		context.Background(),
		&admin_service.ManagerPrimaryKey{
			Id: ManagerID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id Manager resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetManagerList godoc
// @ID get_Manager_list
// @Router /super_admin/manager [GET]
// @Summary Get Manager s List
// @Description  Get Manager s List
// @Tags Manager
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=admin_service.GetListManagerResponse} "GetAllManagerResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetManagerList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ManagerService().GetList(
		context.Background(),
		&admin_service.GetListManagerRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateManager godoc
// @ID update_Manager
// @Router /super_admin/manager/{id} [PUT]
// @Summary Update Manager
// @Description Update Manager
// @Tags Manager
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Param profile body admin_service.UpdateManager true "UpdateManagerRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Manager} "Manager data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateManager(c *gin.Context) {

	var Manager admin_service.UpdateManager
	id := c.Param("id")

	if !util.IsValidUUID(id) {
		h.handlerResponse(c, "Manager id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Manager id"))
		return
	}

	err := c.ShouldBindJSON(&Manager)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Manager.Id = id
	resp, err := h.services.ManagerService().Update(
		c.Request.Context(),
		&Manager,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// DeleteManager godoc
// @ID delete_Manager
// @Router /super_admin/manager/{id} [DELETE]
// @Summary Delete Manager
// @Description Delete Manager
// @Tags Manager
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Manager data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteManager(c *gin.Context) {

	ManagerId := c.Param("id")

	if !util.IsValidUUID(ManagerId) {
		h.handlerResponse(c, "Manager id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Manager id"))
		return
	}

	resp, err := h.services.ManagerService().Delete(
		c.Request.Context(),
		&admin_service.ManagerPrimaryKey{Id: ManagerId},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
