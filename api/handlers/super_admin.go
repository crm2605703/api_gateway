package handlers

import (
	"context"
	"errors"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateSuperAdmin godoc
// @ID create_SuperAdmin
// @Router /super_user/super_admin [POST]
// @Summary Create SuperAdmin
// @Description  Create SuperAdmin
// @Tags SuperAdmin
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param profile body admin_service.CreateSuperAdmin true "CreateSuperAdminRequestBody"
// @Success 200 {object} http.Response{data=admin_service.SuperAdmin} "GetSuperAdminBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSuperAdmin(c *gin.Context) {

	var (
		SuperAdmin admin_service.CreateSuperAdmin
	)
	err := c.ShouldBindJSON(&SuperAdmin)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	//-------------------------------------------------------------------------------

	// insert user branch table
	_, err = h.services.UserBranchService().Create(
		context.Background(),
		&admin_service.CreateUserBranch{BranchId: SuperAdmin.BranchId, Login: SuperAdmin.Login, Password: SuperAdmin.Password},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("login is need unique"))
		return
	}

	// check super admin existent this branch
	spListResp, err := h.services.SuperAdminService().GetList(
		c.Request.Context(),
		&admin_service.GetListSuperAdminRequest{},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	for _, v := range spListResp.SuperAdmins {
		if v.BranchId == SuperAdmin.BranchId {
			h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("super admin existent this branch"))
			return
		}
	}

	resp, err := h.services.SuperAdminService().Create(
		c.Request.Context(),
		&SuperAdmin,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// GetSuperAdminByID godoc
// @ID get_SuperAdmin_by_id
// @Router /super_user/super_admin/{id} [GET]
// @Summary Get SuperAdmin  By ID
// @Description Get SuperAdmin  By ID
// @Tags SuperAdmin
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id query string true "id"
// @Success 200 {object} http.Response{data=admin_service.SuperAdmin} "SuperAdminBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSuperAdminByID(c *gin.Context) {

	SuperAdminID := c.Param("id")

	if !util.IsValidUUID(SuperAdminID) {
		h.handlerResponse(c, "SuperAdmin id is an invalid uuid", http.StatusBadRequest, errors.New("invalid SuperAdmin id"))
		return
	}

	resp, err := h.services.SuperAdminService().GetByID(
		context.Background(),
		&admin_service.SuperAdminPrimaryKey{
			Id: SuperAdminID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id SuperAdmin resposne", http.StatusOK, resp)
}

// GetSuperAdminList godoc
// @ID get_SuperAdmin_list
// @Router /super_user/super_admin [GET]
// @Summary Get SuperAdmin s List
// @Description  Get SuperAdmin s List
// @Tags SuperAdmin
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=admin_service.GetListSuperAdminResponse} "GetAllSuperAdminResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSuperAdminList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SuperAdminService().GetList(
		context.Background(),
		&admin_service.GetListSuperAdminRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}

// UpdateSuperAdmin godoc
// @ID update_SuperAdmin
// @Router /super_user/super_admin/{id} [PUT]
// @Summary Update SuperAdmin
// @Description Update SuperAdmin
// @Tags SuperAdmin
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id query string true "id"
// @Param profile body admin_service.UpdateSuperAdmin true "UpdateSuperAdminRequestBody"
// @Success 200 {object} http.Response{data=admin_service.SuperAdmin} "SuperAdmin data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSuperAdmin(c *gin.Context) {

	var SuperAdmin admin_service.UpdateSuperAdmin
	id := c.Param("id")

	if !util.IsValidUUID(id) {
		h.handlerResponse(c, "SuperAdmin id is an invalid uuid", http.StatusBadRequest, errors.New("invalid SuperAdmin id"))
		return
	}

	err := c.ShouldBindJSON(&SuperAdmin)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	SuperAdmin.Id = id
	resp, err := h.services.SuperAdminService().Update(
		c.Request.Context(),
		&SuperAdmin,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// DeleteSuperAdmin godoc
// @ID delete_SuperAdmin
// @Router /super_user/super_admin/{id} [DELETE]
// @Summary Delete SuperAdmin
// @Description Delete SuperAdmin
// @Tags SuperAdmin
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id query string true "id"
// @Success 200 {object} http.Response{data=object{}} "SuperAdmin data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSuperAdmin(c *gin.Context) {

	SuperAdminId := c.Param("id")

	if !util.IsValidUUID(SuperAdminId) {
		h.handlerResponse(c, "SuperAdmin id is an invalid uuid", http.StatusBadRequest, errors.New("invalid SuperAdmin id"))
		return
	}

	resp, err := h.services.SuperAdminService().Delete(
		c.Request.Context(),
		&admin_service.SuperAdminPrimaryKey{Id: SuperAdminId},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
