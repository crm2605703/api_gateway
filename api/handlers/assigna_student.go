package handlers

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateAssignStudent godoc
// @ID create_AssignStudent
// @Router /mta/assign_student [POST]
// @Summary Create AssignStudent
// @Description  Create AssignStudent
// @Tags AssignStudent
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param profile body admin_service.CreateAssignStudent true "CreateAssignStudentRequestBody"
// @Success 200 {object} http.Response{data=admin_service.AssignStudent} "GetAssignStudentBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateAssignStudent(c *gin.Context) {

	var (
		AssignStudent admin_service.CreateAssignStudent
		login         = c.Query("login")
		password      = c.Query("password")
	)
	err := c.ShouldBindJSON(&AssignStudent)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}
	//------------------------------------------------------------------
	// check to match branch
	usResp, err := h.services.UserBranchService().GetByID(
		context.Background(),
		&admin_service.UserBranchPrimaryKey{Login: login, Password: password},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	} else if usResp.BranchId != AssignStudent.BranchId {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("branch does not to match"))
		return
	}
	//-------------------------------------------------------------------------------

	// get event
	eventResp, err := h.services.EventService().GetByID(
		context.Background(),
		&admin_service.EventPrimaryKey{Id: AssignStudent.EventId},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}
	//--------------------------------------------------------------------------------
	// check event start date, start time
	nowDate, err := time.Parse("2006-01-02", time.Now().Format("2006-01-02"))
	if err != nil {
		h.handlerResponse(c, "error while parsing time", http.StatusInternalServerError, errors.New("error while parsing time"))
		return
	}
	eventDate, err := time.Parse("2006-01-02", eventResp.Day)
	if err != nil {
		h.handlerResponse(c, "error while parsing time", http.StatusInternalServerError, errors.New("error while parsing time"))
		return
	}
	eventTime, err := time.Parse("15:04:05", eventResp.StartTime)
	if err != nil {
		h.handlerResponse(c, "error while parsing time", http.StatusInternalServerError, errors.New("error while parsing time"))
		return
	}
	if time.Now().Add(time.Hour*3).After(eventTime) && nowDate == eventDate {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("expired"))
		return
	}
	//-------------------------------------------------------------------------------------
	resp, err := h.services.AssignStudentService().Create(
		c.Request.Context(),
		&AssignStudent,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// GetAssignStudentByID godoc
// @ID get_AssignStudent_by_id
// @Router /mta/assign_student/{id} [GET]
// @Summary Get AssignStudent  By ID
// @Description Get AssignStudent  By ID
// @Tags AssignStudent
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=admin_service.AssignStudent} "AssignStudentBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetAssignStudentByID(c *gin.Context) {

	AssignStudentID := c.Param("id")
	fmt.Println("------------------------	", AssignStudentID)

	if !util.IsValidUUID(AssignStudentID) {
		h.handlerResponse(c, "AssignStudent id is an invalid uuid", http.StatusBadRequest, errors.New("invalid AssignStudent id"))
		return
	}

	resp, err := h.services.AssignStudentService().GetByID(
		context.Background(),
		&admin_service.AssignStudentPrimaryKey{
			Id: AssignStudentID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id AssignStudent resposne", http.StatusOK, resp)
}

// GetAssignStudentList godoc
// @ID get_AssignStudent_list
// @Router /mta/assign_student [GET]
// @Summary Get AssignStudent s List
// @Description  Get AssignStudent s List
// @Tags AssignStudent
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=admin_service.GetListAssignStudentResponse} "GetAllAssignStudentResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetAssignStudentList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.AssignStudentService().GetList(
		context.Background(),
		&admin_service.GetListAssignStudentRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}

// UpdateAssignStudent godoc
// @ID update_AssignStudent
// @Router /mta/assign_student/{id} [PUT]
// @Summary Update AssignStudent
// @Description Update AssignStudent
// @Tags AssignStudent
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Param profile body admin_service.UpdateAssignStudent true "UpdateAssignStudentRequestBody"
// @Success 200 {object} http.Response{data=admin_service.AssignStudent} "AssignStudent data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateAssignStudent(c *gin.Context) {

	var AssignStudent admin_service.UpdateAssignStudent
	id := c.Param("id")

	if !util.IsValidUUID(id) {
		h.handlerResponse(c, "AssignStudent id is an invalid uuid", http.StatusBadRequest, errors.New("invalid AssignStudent id"))
		return
	}

	err := c.ShouldBindJSON(&AssignStudent)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	AssignStudent.Id = id
	resp, err := h.services.AssignStudentService().Update(
		c.Request.Context(),
		&AssignStudent,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// DeleteAssignStudent godoc
// @ID delete_AssignStudent
// @Router /mta/assign_student/{id} [DELETE]
// @Summary Delete AssignStudent
// @Description Delete AssignStudent
// @Tags AssignStudent
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "AssignStudent data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteAssignStudent(c *gin.Context) {

	AssignStudentId := c.Param("id")

	if !util.IsValidUUID(AssignStudentId) {
		h.handlerResponse(c, "AssignStudent id is an invalid uuid", http.StatusBadRequest, errors.New("invalid AssignStudent id"))
		return
	}

	resp, err := h.services.AssignStudentService().Delete(
		c.Request.Context(),
		&admin_service.AssignStudentPrimaryKey{Id: AssignStudentId},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
