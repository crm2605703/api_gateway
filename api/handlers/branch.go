package handlers

import (
	"context"
	"errors"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateBranch godoc
// @ID create_Branch
// @Router /super_user/branch [POST]
// @Summary Create Branch
// @Description  Create Branch
// @Tags Branch
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param profile body admin_service.CreateBranch true "CreateBranchRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Branch} "GetBranchBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateBranch(c *gin.Context) {

	var Branch admin_service.CreateBranch

	err := c.ShouldBindJSON(&Branch)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.BranchService().Create(
		c.Request.Context(),
		&Branch,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// GetBranchByID godoc
// @ID get_Branch_by_id
// @Router /super_user/branch/{id} [GET]
// @Summary Get Branch  By ID
// @Description Get Branch  By ID
// @Tags Branch
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=admin_service.Branch} "BranchBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetBranchByID(c *gin.Context) {

	BranchID := c.Param("id")
	// fmt.Println("------------------------	", BranchID)

	if !util.IsValidUUID(BranchID) {
		h.handlerResponse(c, "Branch id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Branch id"))
		return
	}

	resp, err := h.services.BranchService().GetByID(
		context.Background(),
		&admin_service.BranchPrimaryKey{
			Id: BranchID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id Branch resposne", http.StatusOK, resp)
}

// GetBranchList godoc
// @ID get_Branch_list
// @Router /super_user/branch [GET]
// @Summary Get Branch s List
// @Description  Get Branch s List
// @Tags Branch
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=admin_service.GetListBranchResponse} "GetAllBranchResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetBranchList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.BranchService().GetList(
		context.Background(),
		&admin_service.GetListBranchRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}

// UpdateBranch godoc
// @ID update_Branch
// @Router /super_user/branch/{id} [PUT]
// @Summary Update Branch
// @Description Update Branch
// @Tags Branch
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Param profile body admin_service.UpdateBranch true "UpdateBranchRequestBody"
// @Success 200 {object} http.Response{data=admin_service.Branch} "Branch data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateBranch(c *gin.Context) {

	var Branch admin_service.UpdateBranch
	id := c.Param("id")

	if !util.IsValidUUID(id) {
		h.handlerResponse(c, "Branch id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Branch id"))
		return
	}

	err := c.ShouldBindJSON(&Branch)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Branch.Id = id
	resp, err := h.services.BranchService().Update(
		c.Request.Context(),
		&Branch,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// DeleteBranch godoc
// @ID delete_Branch
// @Router /super_user/branch/{id} [DELETE]
// @Summary Delete Branch
// @Description Delete Branch
// @Tags Branch
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Branch data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteBranch(c *gin.Context) {

	BranchId := c.Param("id")

	if !util.IsValidUUID(BranchId) {
		h.handlerResponse(c, "Branch id is an invalid uuid", http.StatusBadRequest, errors.New("invalid Branch id"))
		return
	}

	resp, err := h.services.BranchService().Delete(
		c.Request.Context(),
		&admin_service.BranchPrimaryKey{Id: BranchId},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
