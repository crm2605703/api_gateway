package handlers

import (
	"context"
	"errors"
	"net/http"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"user_interest/ui_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateScheduleWeek godoc
// @ID create_ScheduleWeek
// @Router /teacher/schedule_week [POST]
// @Summary Create ScheduleWeek
// @Description  Create ScheduleWeek
// @Tags ScheduleWeek
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param profile body admin_service.CreateScheduleWeek true "CreateScheduleWeekRequestBody"
// @Success 200 {object} http.Response{data=admin_service.ScheduleWeek} "GetScheduleWeekBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateScheduleWeek(c *gin.Context) {

	var (
		ScheduleWeek admin_service.CreateScheduleWeek
		login        = c.Query("login")
		password     = c.Query("password")
	)
	err := c.ShouldBindJSON(&ScheduleWeek)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	//------------------------------------------------------------------
	// check to match branch
	usResp, err := h.services.UserBranchService().GetByID(
		context.Background(),
		&admin_service.UserBranchPrimaryKey{Login: login, Password: password},
	)
	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	} else if usResp.BranchId != ScheduleWeek.BranchId {
		h.handlerResponse(c, "error", http.StatusBadRequest, errors.New("branch does not to match"))
		return
	}
	//-------------------------------------------------------------------------------

	resp, err := h.services.ScheduleWeekService().Create(
		c.Request.Context(),
		&ScheduleWeek,
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "created", http.StatusCreated, resp)
}

// GetScheduleWeekByID godoc
// @ID get_ScheduleWeek_by_id
// @Router /teacherORsuppTeach/schedule_week/{id} [GET]
// @Summary Get ScheduleWeek  By ID
// @Description Get ScheduleWeek  By ID
// @Tags ScheduleWeek
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=admin_service.ScheduleWeek} "ScheduleWeekBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetScheduleWeekByID(c *gin.Context) {

	ScheduleWeekID := c.Param("id")

	if !util.IsValidUUID(ScheduleWeekID) {
		h.handlerResponse(c, "ScheduleWeek id is an invalid uuid", http.StatusBadRequest, errors.New("invalid ScheduleWeek id"))
		return
	}

	resp, err := h.services.ScheduleWeekService().GetByID(
		context.Background(),
		&admin_service.ScheduleWeekPrimaryKey{
			Id: ScheduleWeekID,
		},
	)

	if err != nil {
		h.handlerResponse(c, "error", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(c, "get by id ScheduleWeek resposne", http.StatusOK, resp)
}

// GetScheduleWeekList godoc
// @ID get_ScheduleWeek_list
// @Router /teacherORsuppTeach/schedule_week [GET]
// @Summary Get ScheduleWeek s List
// @Description  Get ScheduleWeek s List
// @Tags ScheduleWeek
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=admin_service.GetListScheduleWeekResponse} "GetAllScheduleWeekResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetScheduleWeekList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handlerResponse(c, "error while get list", http.StatusBadRequest, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handlerResponse(c, " error while getting param", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ScheduleWeekService().GetList(
		context.Background(),
		&admin_service.GetListScheduleWeekRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handlerResponse(c, "", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "getting list", http.StatusOK, resp)
}

// UpdateScheduleWeek godoc
// @ID update_ScheduleWeek
// @Router /teacher/schedule_week/{id} [PUT]
// @Summary Update ScheduleWeek
// @Description Update ScheduleWeek
// @Tags ScheduleWeek
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Param profile body admin_service.UpdateScheduleWeek true "UpdateScheduleWeekRequestBody"
// @Success 200 {object} http.Response{data=admin_service.ScheduleWeek} "ScheduleWeek data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateScheduleWeek(c *gin.Context) {

	var ScheduleWeek admin_service.UpdateScheduleWeek
	id := c.Param("id")

	if !util.IsValidUUID(id) {
		h.handlerResponse(c, "ScheduleWeek id is an invalid uuid", http.StatusBadRequest, errors.New("invalid ScheduleWeek id"))
		return
	}

	err := c.ShouldBindJSON(&ScheduleWeek)
	if err != nil {
		h.handlerResponse(c, "should bind json", http.StatusBadRequest, err.Error())
		return
	}

	ScheduleWeek.Id = id
	resp, err := h.services.ScheduleWeekService().Update(
		c.Request.Context(),
		&ScheduleWeek,
	)

	if err != nil {
		h.handlerResponse(c, "error while updating", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "updated", http.StatusOK, resp)
}

// DeleteScheduleWeek godoc
// @ID delete_ScheduleWeek
// @Router /teacher/schedule_week/{id} [DELETE]
// @Summary Delete ScheduleWeek
// @Description Delete ScheduleWeek
// @Tags ScheduleWeek
// @Accept json
// @Produce json
// @Param login query string true "login"
// @Param password query string true "password"
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "ScheduleWeek data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteScheduleWeek(c *gin.Context) {

	ScheduleWeekId := c.Param("id")

	if !util.IsValidUUID(ScheduleWeekId) {
		h.handlerResponse(c, "ScheduleWeek id is an invalid uuid", http.StatusBadRequest, errors.New("invalid ScheduleWeek id"))
		return
	}

	resp, err := h.services.ScheduleWeekService().Delete(
		c.Request.Context(),
		&admin_service.ScheduleWeekPrimaryKey{Id: ScheduleWeekId},
	)

	if err != nil {
		h.handlerResponse(c, "error while deleting", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "deleted", http.StatusNoContent, resp)
}
