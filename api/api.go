package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"user_interest/ui_go_api_gateway/api/docs"
	"user_interest/ui_go_api_gateway/api/handlers"

	"user_interest/ui_go_api_gateway/config"
)

func SetUpAPI(r *gin.Engine, h handlers.Handler, cfg config.Config) {
	docs.SwaggerInfo.Title = cfg.ServiceName
	docs.SwaggerInfo.Version = cfg.Version
	docs.SwaggerInfo.Schemes = []string{cfg.HTTPScheme}

	r.Use(MaxAllowed(5000))

	//-------------------------------Middlwere-----------------------------------------

	// check super user
	superUser := r.Group("/super_user")
	superUser.Use(h.SuperUserCheck())

	// check super admin
	superAdmin := r.Group("/super_admin")
	superAdmin.Use(h.SuperAdminCheck())

	// check manager, super admin
	superAdminORmanager := r.Group("/superAdminORmanager")
	superAdminORmanager.Use(h.SuperAdminOrManagerCheck())

	// check teacher, super admin
	teacher := r.Group("/teacher")
	teacher.Use(h.TeacherCheck())

	// check teacher, support teacher, super admin
	teacherORsuppTeach := r.Group("/teacherORsuppTeach")
	teacherORsuppTeach.Use(h.TeacherOrSupprtCheck())

	// check teacher, support teacher, super admin
	student := r.Group("/student")
	student.Use(h.Student())

	// check administration, super admin
	administration := r.Group("/administration")
	administration.Use(h.AdministrationCheck())

	// check teacher, support teacher, super admin
	managerORteacher := r.Group("/manegORteach")
	managerORteacher.Use(h.ManagerORteacherCheck())

	// check teacher, support teacher, student, super admin
	mts := r.Group("/mts")
	mts.Use(h.MTSCheck())

	// check teacher, support teacher, admin, super admin
	mta := r.Group("/mta")
	mta.Use(h.MTACheck())
	//-----------------------------------------------------------------------------

	//-------------------------------Router----------------------------------------

	administration.POST("/payment", h.CreatePayment)
	administration.GET("/payment/:id", h.GetPaymentByID)
	administration.GET("/payment", h.GetPaymentList)
	administration.PUT("/payment/:id", h.UpdatePayment)
	administration.DELETE("/payment/:id", h.DeletePayment)

	administration.POST("/event", h.CreateEvent)
	administration.GET("/event/:id", h.GetEventByID)
	administration.GET("/event", h.GetEventList)
	administration.PUT("/event/:id", h.UpdateEvent)
	administration.DELETE("/event/:id", h.DeleteEvent)

	superAdminORmanager.POST("/group", h.CreateGroup)
	managerORteacher.GET("/group/:id", h.GetGroupByID)
	superAdminORmanager.GET("/group", h.GetGroupList)
	superAdminORmanager.PUT("/group/:id", h.UpdateGroup)
	superAdminORmanager.DELETE("/group/:id", h.DeleteGroup)

	teacher.POST("/journal", h.CreateJournal)
	managerORteacher.GET("/journal/:id", h.GetJournalByID)
	managerORteacher.GET("/journal", h.GetJournalList)
	teacher.PUT("/journal/:id", h.UpdateJournal)
	teacher.DELETE("/journal/:id", h.DeleteJournal)

	teacher.POST("/lesson", h.CreateLesson)
	teacherORsuppTeach.GET("/lesson/:id", h.GetLessonByID)
	teacherORsuppTeach.GET("/lesson", h.GetLessonList)
	teacher.PUT("/lesson/:id", h.UpdateLesson)
	teacher.DELETE("/lesson/:id", h.DeleteLesson)

	superUser.POST("/super_admin", h.CreateSuperAdmin)
	superUser.GET("/super_admin/:id", h.GetSuperAdminByID)
	superUser.GET("/super_admin", h.GetSuperAdminList)
	superUser.PUT("/super_admin/:id", h.UpdateSuperAdmin)
	superUser.DELETE("/super_admin/:id", h.DeleteSuperAdmin)

	superUser.POST("/branch", h.CreateBranch)
	superUser.GET("/branch/:id", h.GetBranchByID)
	superUser.GET("/branch", h.GetBranchList)
	superUser.PUT("/branch/:id", h.UpdateBranch)
	superUser.DELETE("/branch/:id", h.DeleteBranch)

	teacher.POST("/task", h.CreateTask)
	teacherORsuppTeach.GET("/task/:id", h.GetTaskByID)
	teacherORsuppTeach.GET("/task", h.GetTaskList)
	teacher.PUT("/task/:id", h.UpdateTask)
	teacher.DELETE("/task/:id", h.DeleteTask)

	teacher.POST("/to_do_task", h.CreateToDoTask)
	teacherORsuppTeach.GET("/to_do_task/:id", h.GetToDoTaskByID)
	teacherORsuppTeach.GET("/to_do_task", h.GetToDoTaskList)
	teacher.PUT("/to_do_task/:id", h.UpdateToDoTask)
	teacher.DELETE("/to_do_task/:id", h.DeleteToDoTask)

	superAdmin.POST("/manager", h.CreateManager)
	superAdmin.GET("/manager/:id", h.GetManagerByID)
	superAdmin.GET("/manager", h.GetManagerList)
	superAdmin.PUT("/manager/:id", h.UpdateManager)
	superAdmin.DELETE("/manager/:id", h.DeleteManager)

	superAdminORmanager.POST("/teacher", h.CreateTeacher)
	superAdminORmanager.GET("/teacher/:id", h.GetTeacherByID)
	superAdminORmanager.GET("/teacher", h.GetTeacherList)
	superAdminORmanager.PUT("/teacher/:id", h.UpdateTeacher)
	superAdminORmanager.DELETE("/teacher/:id", h.DeleteTeacher)
	teacher.GET("/get_teacher/:branch_id", h.GetTeacher)
	teacher.GET("/get_teacher_personal/:teacher_id", h.TeacherReport)

	superAdminORmanager.POST("/support_teacher", h.CreateSupportTeacher)
	superAdminORmanager.GET("/support_teacher/:id", h.GetSupportTeacherByID)
	superAdminORmanager.GET("/support_teacher", h.GetSupportTeacherList)
	superAdminORmanager.PUT("/support_teacher/:id", h.UpdateSupportTeacher)
	superAdminORmanager.DELETE("/support_teacher/:id", h.DeleteSupportTeacher)
	teacherORsuppTeach.GET("/get_support_teacher/:branch_id", h.GetSupportTeacher)
	teacherORsuppTeach.GET("/get_pesonal_support_teacher_by_id/:support_teacher_id", h.SupportTeacherReport)

	mta.POST("/assign_student", h.CreateAssignStudent)
	mts.GET("/assign_student/:id", h.GetAssignStudentByID)
	mta.GET("/assign_student", h.GetAssignStudentList)
	mta.PUT("/assign_student/:id", h.UpdateAssignStudent)
	mta.DELETE("/assign_student/:id", h.DeleteAssignStudent)

	mta.POST("/student", h.CreateStudent)
	mts.GET("/student/:id", h.GetStudentByID)
	mta.GET("/student", h.GetStudentList)
	mta.PUT("/student/:id", h.UpdateStudent)
	mta.DELETE("/student/:id", h.DeleteStudent)
	student.GET("/get_student/:branch_id", h.GetStudent)
	student.GET("/get_student_personal/:student_id", h.StudentReport)

	superAdminORmanager.POST("/administration", h.CreateAdministration)
	superAdminORmanager.GET("/administration/:id", h.GetAdministrationByID)
	superAdminORmanager.GET("/administration", h.GetAdministrationList)
	superAdminORmanager.PUT("/administration/:id", h.UpdateAdministration)
	superAdminORmanager.DELETE("/administration/:id", h.DeleteAdministration)
	administration.GET("/get_admin/:branch_id", h.GetAdministrator)

	teacher.POST("/schedule_week", h.CreateScheduleWeek)
	teacherORsuppTeach.GET("/schedule_week/:id", h.GetScheduleWeekByID)
	teacherORsuppTeach.GET("/schedule_week", h.GetScheduleWeekList)
	teacher.PUT("/schedule_week/:id", h.UpdateScheduleWeek)
	teacher.DELETE("/schedule_week/:id", h.DeleteScheduleWeek)
	//------------------------------------------------------------------------------------

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}
