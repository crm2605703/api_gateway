package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

const (
	// DebugMode indicates service mode is debug.
	DebugMode = "debug"
	// TestMode indicates service mode is test.
	TestMode = "test"
	// ReleaseMode indicates service mode is release.
	ReleaseMode = "release"
)

type Config struct {
	ServiceName string
	Environment string // debug, test, release
	Version     string

	ServiceHost string
	HTTPPort    string
	HTTPScheme  string
	Domain      string

	DefaultOffset string
	DefaultLimit  string

	AdminServiceHost string
	AdminGRPCPort    string

	ContentServiceHost string
	ContentGRPCPort    string

	WebServiceHost string
	WebGRPCPort    string

	DefaultBarCode string
	DefaultSaleId  string

	AuthServiceHost string
	AuthGRPCPort    string
	SecretKey       string

	SuperLogin    string
	SuperPassword string
}

// Load ...
func Load() Config {
	if err := godotenv.Load("/go-api-gateway.env"); err != nil {
		fmt.Println("No .env file found")
	}

	config := Config{}

	config.ServiceName = cast.ToString(getOrReturnDefaultValue("SERVICE_NAME", "go_api_gateway"))
	config.Environment = cast.ToString(getOrReturnDefaultValue("ENVIRONMENT", DebugMode))
	config.Version = cast.ToString(getOrReturnDefaultValue("VERSION", "1.0"))

	config.ServiceHost = cast.ToString(getOrReturnDefaultValue("SERVICE_HOST", "localhost"))
	config.HTTPPort = cast.ToString(getOrReturnDefaultValue("HTTP_PORT", ":8080"))
	config.HTTPScheme = cast.ToString(getOrReturnDefaultValue("HTTP_SCHEME", "http"))
	config.Domain = cast.ToString(getOrReturnDefaultValue("DOMAIN", "localhost:8080"))

	config.DefaultOffset = cast.ToString(getOrReturnDefaultValue("DEFAULT_OFFSET", "0"))
	config.DefaultLimit = cast.ToString(getOrReturnDefaultValue("DEFAULT_LIMIT", "10"))

	config.DefaultBarCode = cast.ToString(getOrReturnDefaultValue("BAR_CODE", ""))
	config.DefaultSaleId = cast.ToString(getOrReturnDefaultValue("SALE_ID", ""))

	config.AdminServiceHost = cast.ToString(getOrReturnDefaultValue("ADMIN_SERVICE_HOST", "localhost"))
	config.AdminGRPCPort = cast.ToString(getOrReturnDefaultValue("ADMIN_GRPC_PORT", ":8081"))

	config.WebServiceHost = cast.ToString(getOrReturnDefaultValue("WEB_SERVICE_HOST", "localhost"))
	config.WebGRPCPort = cast.ToString(getOrReturnDefaultValue("WEB_GRPC_PORT", ":8082"))

	config.SecretKey = cast.ToString(getOrReturnDefaultValue("SECRET_KEY", "market"))

	config.SuperLogin = cast.ToString(getOrReturnDefaultValue("SUPER_LOGIN", "login123"))
	config.SuperPassword = cast.ToString(getOrReturnDefaultValue("SUPER_PASSWORD", "password123"))
	return config
}

func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{} {
	val, exists := os.LookupEnv(key)

	if exists {
		return val
	}

	return defaultValue
}
