package services

import (
	"user_interest/ui_go_api_gateway/config"
	"user_interest/ui_go_api_gateway/genproto/admin_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	// admin service
	AdministrationService() admin_service.AdministrationServiceClient
	AssignStudentService() admin_service.AssignStudentServiceClient
	BranchService() admin_service.BranchServiceClient
	EventService() admin_service.EventServiceClient
	GroupService() admin_service.GroupServiceClient
	JournalService() admin_service.JournalServiceClient
	LessonService() admin_service.LessonServiceClient
	ManagerService() admin_service.ManagerServiceClient
	ScheduleWeekService() admin_service.ScheduleWeekServiceClient
	StudentService() admin_service.StudentServiceClient
	SuperAdminService() admin_service.SuperAdminServiceClient
	SupportTeacherService() admin_service.SupportTeacherServiceClient
	TaskService() admin_service.TaskServiceClient
	TeacherService() admin_service.TeacherServiceClient
	ToDoTaskService() admin_service.ToDoTaskServiceClient
	UserBranchService() admin_service.UserBranchServiceClient
	PaymentService() admin_service.PaymentServiceClient
	ReportService() admin_service.ReportServiceClient
}

type grpcClients struct {
	// admin service
	administrationService admin_service.AdministrationServiceClient
	assignStudentService  admin_service.AssignStudentServiceClient
	branchService         admin_service.BranchServiceClient
	eventService          admin_service.EventServiceClient
	groupService          admin_service.GroupServiceClient
	journalService        admin_service.JournalServiceClient
	lessonService         admin_service.LessonServiceClient
	managerService        admin_service.ManagerServiceClient
	scheduleWeekService   admin_service.ScheduleWeekServiceClient
	studentService        admin_service.StudentServiceClient
	superAdminService     admin_service.SuperAdminServiceClient
	supportTeacherService admin_service.SupportTeacherServiceClient
	taskService           admin_service.TaskServiceClient
	teacherService        admin_service.TeacherServiceClient
	toDoTaskService       admin_service.ToDoTaskServiceClient
	userBranchService     admin_service.UserBranchServiceClient
	paymentService        admin_service.PaymentServiceClient
	reportService         admin_service.ReportServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// Admin Service...
	connAdminService, err := grpc.Dial(
		cfg.AdminServiceHost+cfg.AdminGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		//admin service
		administrationService: admin_service.NewAdministrationServiceClient(connAdminService),
		assignStudentService:  admin_service.NewAssignStudentServiceClient(connAdminService),
		branchService:         admin_service.NewBranchServiceClient(connAdminService),
		eventService:          admin_service.NewEventServiceClient(connAdminService),
		groupService:          admin_service.NewGroupServiceClient(connAdminService),
		journalService:        admin_service.NewJournalServiceClient(connAdminService),
		lessonService:         admin_service.NewLessonServiceClient(connAdminService),
		managerService:        admin_service.NewManagerServiceClient(connAdminService),
		scheduleWeekService:   admin_service.NewScheduleWeekServiceClient(connAdminService),
		studentService:        admin_service.NewStudentServiceClient(connAdminService),
		superAdminService:     admin_service.NewSuperAdminServiceClient(connAdminService),
		supportTeacherService: admin_service.NewSupportTeacherServiceClient(connAdminService),
		taskService:           admin_service.NewTaskServiceClient(connAdminService),
		teacherService:        admin_service.NewTeacherServiceClient(connAdminService),
		toDoTaskService:       admin_service.NewToDoTaskServiceClient(connAdminService),
		userBranchService:     admin_service.NewUserBranchServiceClient(connAdminService),
		paymentService:        admin_service.NewPaymentServiceClient(connAdminService),
		reportService:         admin_service.NewReportServiceClient(connAdminService),
	}, nil
}

// admin service methods
func (g *grpcClients) AdministrationService() admin_service.AdministrationServiceClient {
	return g.administrationService
}

func (g *grpcClients) AssignStudentService() admin_service.AssignStudentServiceClient {
	return g.assignStudentService
}

func (g *grpcClients) BranchService() admin_service.BranchServiceClient {
	return g.branchService
}

func (g *grpcClients) EventService() admin_service.EventServiceClient {
	return g.eventService
}

func (g *grpcClients) GroupService() admin_service.GroupServiceClient {
	return g.groupService
}

func (g *grpcClients) JournalService() admin_service.JournalServiceClient {
	return g.journalService
}

func (g *grpcClients) LessonService() admin_service.LessonServiceClient {
	return g.lessonService
}

func (g *grpcClients) ManagerService() admin_service.ManagerServiceClient {
	return g.managerService
}

func (g *grpcClients) ScheduleWeekService() admin_service.ScheduleWeekServiceClient {
	return g.scheduleWeekService
}

func (g *grpcClients) StudentService() admin_service.StudentServiceClient {
	return g.studentService
}

func (g *grpcClients) SuperAdminService() admin_service.SuperAdminServiceClient {
	return g.superAdminService
}

func (g *grpcClients) SupportTeacherService() admin_service.SupportTeacherServiceClient {
	return g.supportTeacherService
}

func (g *grpcClients) TaskService() admin_service.TaskServiceClient {
	return g.taskService
}

func (g *grpcClients) TeacherService() admin_service.TeacherServiceClient {
	return g.teacherService
}

func (g *grpcClients) ToDoTaskService() admin_service.ToDoTaskServiceClient {
	return g.toDoTaskService
}

func (g *grpcClients) UserBranchService() admin_service.UserBranchServiceClient {
	return g.userBranchService
}

func (g *grpcClients) PaymentService() admin_service.PaymentServiceClient {
	return g.paymentService
}

func (g *grpcClients) ReportService() admin_service.ReportServiceClient {
	return g.reportService
}
